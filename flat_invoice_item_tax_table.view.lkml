view: flat_invoice_item_tax_table {
  sql_table_name: flat_tables.flat_invoice_item_tax_table ;;

  dimension: asn_id {
    type: number
    sql: ${TABLE}.asn_id ;;
  }

  dimension: consignment_id {
    type: number
    sql: ${TABLE}.consignment_id ;;
  }

  dimension_group: date_add {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.date_add ;;
  }

  dimension: doc_req_id {
    type: number
    sql: ${TABLE}.doc_req_id ;;
  }

  dimension: doc_type {
    type: string
    sql: ${TABLE}.doc_type ;;
  }

  dimension: document_id {
    type: number
    sql: ${TABLE}.document_id ;;
  }

  dimension: gst_tax {
    type: number
    sql: ${TABLE}.gst_tax ;;
  }

  dimension: po_item_id {
    type: number
    sql: ${TABLE}.po_item_id ;;
  }

  dimension: post_tax_price {
    type: number
    sql: ${TABLE}.post_tax_price ;;
  }

  dimension: pre_tax_price {
    type: number
    sql: ${TABLE}.pre_tax_price ;;
  }

  dimension: product_name {
    type: string
    sql: ${TABLE}.product_name ;;
  }

  dimension: quantity {
    type: number
    sql: ${TABLE}.quantity ;;
  }

  dimension: request_type {
    type: string
    sql: ${TABLE}.request_type ;;
  }

  dimension: sku_code {
    type: string
    sql: ${TABLE}.sku_code ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  measure: count {
    type: count
    drill_fields: [product_name]
  }
}
