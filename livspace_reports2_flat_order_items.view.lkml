view: livspace_reports2_flat_order_items {
  sql_table_name: flat_tables.livspace_reports2_flat_order_items ;;

  dimension: current_owner {
    type: number
    sql: ${TABLE}.current_owner ;;
  }

  dimension: detail_active {
    type: number
    sql: ${TABLE}.detail_active ;;
  }

  dimension: group_name {
    type: string
    sql: ${TABLE}.group_name ;;
  }

  dimension: group_sku_code {
    type: string
    sql: ${TABLE}.group_sku_code ;;
  }

  dimension: grouped_sku {
    type: number
    sql: ${TABLE}.grouped_sku ;;
  }

  dimension: id_mapping {
    type: number
    sql: ${TABLE}.id_mapping ;;
  }

  dimension: id_order {
    type: number
    sql: ${TABLE}.id_order ;;
  }

  dimension: id_order_detail {
    type: number
    sql: ${TABLE}.id_order_detail ;;
  }

  dimension: mapped_discount {
    type: number
    sql: ${TABLE}.mapped_discount ;;
  }

  dimension: mapped_handling {
    type: number
    sql: ${TABLE}.mapped_handling ;;
  }

  dimension: mapped_quantity {
    type: number
    sql: ${TABLE}.mapped_quantity ;;
  }

  dimension: mapping_active {
    type: number
    sql: ${TABLE}.mapping_active ;;
  }

  dimension: po_item_id {
    type: number
    sql: ${TABLE}.po_item_id ;;
  }

  dimension: poitem_id_sp {
    type: number
    value_format_name: id
    sql: ${TABLE}.poitem_id_sp ;;
  }

  dimension: poitem_id_sp_wt {
    type: number
    value_format_name: id
    sql: ${TABLE}.poitem_id_sp_wt ;;
  }

  dimension: price_no_tax {
    type: number
    sql: ${TABLE}.price_no_tax ;;
  }

  dimension: product_id {
    type: number
    sql: ${TABLE}.product_id ;;
  }

  dimension: product_name {
    type: string
    sql: ${TABLE}.product_name ;;
  }

  dimension_group: shipment_requested {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.shipment_requested_date ;;
  }

  dimension: sku {
    type: string
    sql: ${TABLE}.sku ;;
  }

  dimension: tax_percent {
    type: number
    sql: ${TABLE}.tax_percent ;;
  }

  dimension: total_order_quantity {
    type: number
    sql: ${TABLE}.total_order_quantity ;;
  }

  measure: count {
    type: count
    drill_fields: [group_name, product_name]
  }
}
