view: flat_grouped_wms_item_grn {
  sql_table_name: flat_tables.flat_grouped_wms_item_grn ;;

  dimension_group: first_grn {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.first_grn_date ;;
  }

  dimension_group: last_grn {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_grn_date ;;
  }

  dimension: po_item_id {
    type: number
    sql: ${TABLE}.po_item_id ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
