view: livspace_reports2_flat_po_item_misc {
  sql_table_name: flat_tables.livspace_reports2_flat_po_item_misc ;;

  dimension: extra_cost {
    type: number
    sql: ${TABLE}.extra_cost ;;
  }

  dimension: id_po {
    type: number
    sql: ${TABLE}.id_po ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
