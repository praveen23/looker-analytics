view: flat_consignments {
  sql_table_name: flat_tables.flat_consignments ;;

  dimension: allocation_type {
    type: string
    sql: ${TABLE}.allocation_type ;;
  }

  dimension: asn_acknowledged {
    type: number
    sql: ${TABLE}.asn_acknowledged ;;
  }

  dimension_group: asn_created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.asn_created_at ;;
  }

  dimension: asn_id {
    type: number
    sql: ${TABLE}.asn_id ;;
  }

  dimension_group: asn_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.asn_updated_at ;;
  }

  dimension: auto_generated {
    type: number
    sql: ${TABLE}.auto_generated ;;
  }

  dimension: consignment_archived {
    type: number
    sql: ${TABLE}.consignment_archived ;;
  }

  dimension: consignment_desc {
    type: string
    sql: ${TABLE}.consignment_desc ;;
  }

  dimension_group: consignment_end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.consignment_end ;;
  }

  dimension: consignment_id {
    type: number
    sql: ${TABLE}.consignment_id ;;
  }

  dimension_group: consignment_start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.consignment_start ;;
  }

  dimension: destination_object_id {
    type: number
    sql: ${TABLE}.destination_object_id ;;
  }

  dimension: destination_type {
    type: string
    sql: ${TABLE}.destination_type ;;
  }

  dimension: grn_acknowledged {
    type: number
    sql: ${TABLE}.grn_acknowledged ;;
  }

  dimension: order_id {
    type: number
    sql: ${TABLE}.order_id ;;
  }

  dimension: po_item_id {
    type: number
    sql: ${TABLE}.po_item_id ;;
  }

  dimension: quantity {
    type: number
    sql: ${TABLE}.quantity ;;
  }

  dimension: sku_code {
    type: string
    sql: ${TABLE}.sku_code ;;
  }

  dimension: source_object_id {
    type: number
    sql: ${TABLE}.source_object_id ;;
  }

  dimension: source_type {
    type: string
    sql: ${TABLE}.source_type ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
