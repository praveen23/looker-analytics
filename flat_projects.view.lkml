view: flat_projects {
  sql_table_name: flat_tables.flat_projects ;;

  dimension: address {
    type: string
    sql: ${TABLE}.address ;;
  }

  dimension_group: awaiting_10_percent {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.awaiting_10_percent_date ;;
  }

  dimension: brief_scope {
    type: string
    sql: ${TABLE}.brief_scope ;;
  }

  dimension_group: briefing_done {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.briefing_done_date ;;
  }

  dimension: budget_max {
    type: number
    sql: ${TABLE}.budget_max ;;
  }

  dimension: budget_min {
    type: number
    sql: ${TABLE}.budget_min ;;
  }

  dimension: customer_display_name {
    type: string
    sql: ${TABLE}.customer_display_name ;;
  }

  dimension: customer_email {
    type: string
    sql: ${TABLE}.customer_email ;;
  }

  dimension: customer_phone {
    type: string
    sql: ${TABLE}.customer_phone ;;
  }

  dimension_group: design_in_progress {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.design_in_progress ;;
  }

  dimension: design_manager {
    type: string
    sql: ${TABLE}.design_manager ;;
  }

  dimension: design_manager_email {
    type: string
    sql: ${TABLE}.design_manager_email ;;
  }

  dimension: fifty_percent_amount {
    type: number
    sql: ${TABLE}.fifty_percent_amount ;;
  }

  dimension_group: fifty_percent_collected {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.fifty_percent_collected_date ;;
  }

  dimension: full_amount {
    type: number
    sql: ${TABLE}.full_amount ;;
  }

  dimension: general_manager {
    type: string
    sql: ${TABLE}.general_manager ;;
  }

  dimension: general_manager_email {
    type: string
    sql: ${TABLE}.general_manager_email ;;
  }

  dimension: inactivation_reason {
    type: string
    sql: ${TABLE}.inactivation_reason ;;
  }

  dimension: is_awaiting_ten_percent {
    type: number
    sql: ${TABLE}.is_awaiting_ten_percent ;;
  }

  dimension: is_converted {
    type: number
    sql: ${TABLE}.is_converted ;;
  }

  dimension: is_proposal_present {
    type: number
    sql: ${TABLE}.is_proposal_present ;;
  }

  dimension: is_qualified {
    type: number
    sql: ${TABLE}.is_qualified ;;
  }

  dimension: is_test_project {
    type: number
    sql: ${TABLE}.is_test_project ;;
  }

  dimension: last_note {
    type: string
    sql: ${TABLE}.last_note ;;
  }

  dimension: lead_medium_id {
    type: number
    sql: ${TABLE}.lead_medium_id ;;
  }

  dimension: lead_medium_name {
    type: string
    sql: ${TABLE}.lead_medium_name ;;
  }

  dimension: lead_source {
    type: string
    sql: ${TABLE}.lead_source ;;
  }

  dimension_group: order_confirmed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.order_confirmed_date ;;
  }

  dimension: parent_city {
    type: string
    sql: ${TABLE}.parent_city ;;
  }

  dimension: postcode {
    type: string
    sql: ${TABLE}.postcode ;;
  }

  dimension: primary_cm {
    type: string
    sql: ${TABLE}.primary_cm ;;
  }

  dimension: primary_cm_email {
    type: string
    sql: ${TABLE}.primary_cm_email ;;
  }

  dimension: primary_designer {
    type: string
    sql: ${TABLE}.primary_designer ;;
  }

  dimension: primary_gm {
    type: string
    sql: ${TABLE}.primary_gm ;;
  }

  dimension: priority {
    type: string
    sql: ${TABLE}.priority ;;
  }

  dimension: project_city {
    type: string
    sql: ${TABLE}.project_city ;;
  }

  dimension_group: project_created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.project_created_at ;;
  }

  dimension: project_created_id {
    type: number
    sql: ${TABLE}.project_created_id ;;
  }

  dimension: project_designer {
    type: string
    sql: ${TABLE}.project_designer ;;
  }

  dimension: project_designer_email {
    type: string
    sql: ${TABLE}.project_designer_email ;;
  }

  dimension: project_display_name {
    type: string
    sql: ${TABLE}.project_display_name ;;
  }

  dimension: project_id {
    type: number
    sql: ${TABLE}.project_id ;;
  }

  dimension: project_manager {
    type: string
    sql: ${TABLE}.project_manager ;;
  }

  dimension: project_manager_email {
    type: string
    sql: ${TABLE}.project_manager_email ;;
  }

  dimension: project_pincode {
    type: string
    sql: ${TABLE}.project_pincode ;;
  }

  dimension: project_property_name {
    type: string
    sql: ${TABLE}.project_property_name ;;
  }

  dimension: project_property_type {
    type: string
    sql: ${TABLE}.project_property_type ;;
  }

  dimension: project_service_type {
    type: string
    sql: ${TABLE}.project_service_type ;;
  }

  dimension: project_stage {
    type: string
    sql: ${TABLE}.project_stage ;;
  }

  dimension: project_stage_weight {
    type: number
    sql: ${TABLE}.project_stage_weight ;;
  }

  dimension: project_status {
    type: string
    sql: ${TABLE}.project_status ;;
  }

  dimension_group: project_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.project_updated_at ;;
  }

  dimension: project_updated_id {
    type: number
    sql: ${TABLE}.project_updated_id ;;
  }

  dimension_group: prospective_lead {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.prospective_lead_date ;;
  }

  dimension: source {
    type: string
    sql: ${TABLE}.source ;;
  }

  dimension: status_change_reason {
    type: string
    sql: ${TABLE}.status_change_reason ;;
  }

  dimension: ten_percent_amount {
    type: number
    sql: ${TABLE}.ten_percent_amount ;;
  }

  dimension_group: ten_percent_collected {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.ten_percent_collected_date ;;
  }

  measure: count {
    type: count
    drill_fields: [project_display_name, lead_medium_name, project_property_name, customer_display_name]
  }
}
