view: livspace_reports2_flat_vms_poitem_asn {
  sql_table_name: flat_tables.livspace_reports2_flat_vms_poitem_asn ;;

  dimension_group: asn_created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.asn_created_at ;;
  }

  dimension: asn_number {
    type: number
    sql: ${TABLE}.asn_number ;;
  }

  dimension: asn_status {
    type: string
    sql: ${TABLE}.asn_status ;;
  }

  dimension_group: asn_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.asn_updated_at ;;
  }

  dimension: package_id {
    type: string
    sql: ${TABLE}.package_id ;;
  }

  dimension: po_item_id {
    type: number
    sql: ${TABLE}.po_item_id ;;
  }

  dimension: vendor_facility_id {
    type: number
    sql: ${TABLE}.vendor_facility_id ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
