view: livspace_reports2_flat_project_settings {
  sql_table_name: flat_tables.livspace_reports2_flat_project_settings ;;

  dimension: cm_email {
    type: string
    sql: ${TABLE}.cm_email ;;
  }

  dimension: designer_email {
    type: string
    sql: ${TABLE}.designer_email ;;
  }

  dimension: gm_email {
    type: string
    sql: ${TABLE}.gm_email ;;
  }

  dimension: pm_email {
    type: string
    sql: ${TABLE}.pm_email ;;
  }

  dimension: primary_cm {
    type: string
    sql: ${TABLE}.primary_cm ;;
  }

  dimension: primary_cm_id {
    type: number
    sql: ${TABLE}.primary_cm_id ;;
  }

  dimension: primary_designer {
    type: string
    sql: ${TABLE}.primary_designer ;;
  }

  dimension: primary_designer_id {
    type: number
    sql: ${TABLE}.primary_designer_id ;;
  }

  dimension: primary_gm {
    type: string
    sql: ${TABLE}.primary_gm ;;
  }

  dimension: primary_gm_id {
    type: number
    sql: ${TABLE}.primary_gm_id ;;
  }

  dimension: primary_pm {
    type: string
    sql: ${TABLE}.primary_pm ;;
  }

  dimension: primary_pm_id {
    type: number
    sql: ${TABLE}.primary_pm_id ;;
  }

  dimension: project_id {
    type: number
    sql: ${TABLE}.project_id ;;
  }

  dimension: source {
    type: string
    sql: ${TABLE}.source ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
