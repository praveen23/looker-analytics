view: livspace_reports2_flat_poitem_to_poitem {
  sql_table_name: flat_tables.livspace_reports2_flat_poitem_to_poitem ;;

  dimension: allocated_po_item_id {
    type: number
    sql: ${TABLE}.allocated_po_item_id ;;
  }

  dimension: allocated_to_id {
    type: number
    sql: ${TABLE}.allocated_to_id ;;
  }

  dimension: allocation_type {
    type: string
    sql: ${TABLE}.allocation_type ;;
  }

  dimension: build_type {
    type: string
    sql: ${TABLE}.build_type ;;
  }

  dimension: created_by_id {
    type: number
    sql: ${TABLE}.created_by_id ;;
  }

  dimension: po_item_id {
    type: number
    sql: ${TABLE}.po_item_id ;;
  }

  dimension: primary_allocation_type {
    type: string
    sql: ${TABLE}.primary_allocation_type ;;
  }

  dimension: sku_quantity {
    type: number
    sql: ${TABLE}.sku_quantity ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
