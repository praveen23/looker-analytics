view: livspace_reports2_flat_wms_items {
  sql_table_name: flat_tables.livspace_reports2_flat_wms_items ;;

  dimension: allocation_type {
    type: string
    sql: ${TABLE}.allocation_type ;;
  }

  dimension: bin_id {
    type: number
    sql: ${TABLE}.bin_id ;;
  }

  dimension: code {
    type: string
    sql: ${TABLE}.code ;;
  }

  dimension: first_grn_number {
    type: number
    sql: ${TABLE}.first_grn_number ;;
  }

  dimension_group: first_grn_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.first_grn_updated_at ;;
  }

  dimension: latest_grn_number {
    type: number
    sql: ${TABLE}.latest_grn_number ;;
  }

  dimension_group: latest_grn_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.latest_grn_updated_at ;;
  }

  dimension: po_item_id {
    type: string
    sql: ${TABLE}.po_item_id ;;
  }

  dimension: quantity {
    type: number
    sql: ${TABLE}.quantity ;;
  }

  dimension: sku_code {
    type: string
    sql: ${TABLE}.sku_code ;;
  }

  dimension: sku_name {
    type: string
    sql: ${TABLE}.sku_name ;;
  }

  dimension: status {
    type: number
    sql: ${TABLE}.status ;;
  }

  dimension: warehouse {
    type: string
    sql: ${TABLE}.warehouse ;;
  }

  dimension: warehouse_id {
    type: number
    sql: ${TABLE}.warehouse_id ;;
  }

  measure: count {
    type: count
    drill_fields: [sku_name]
  }
}
