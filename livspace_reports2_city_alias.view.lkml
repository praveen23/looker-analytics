view: livspace_reports2_city_alias {
  sql_table_name: flat_tables.livspace_reports2_city_alias ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: alias {
    type: string
    sql: ${TABLE}.alias ;;
  }

  dimension: city {
    type: string
    sql: ${TABLE}.city ;;
  }

  measure: count {
    type: count
    drill_fields: [id]
  }
}
