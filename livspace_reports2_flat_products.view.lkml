view: livspace_reports2_flat_products {
  sql_table_name: flat_tables.livspace_reports2_flat_products ;;

  dimension: available_offline {
    type: number
    sql: ${TABLE}.available_offline ;;
  }

  dimension: category_sku {
    type: number
    sql: ${TABLE}.category_sku ;;
  }

  dimension: depth {
    type: number
    sql: ${TABLE}.depth ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}.description ;;
  }

  dimension: description_short {
    type: string
    sql: ${TABLE}.description_short ;;
  }

  dimension: feature_name {
    type: string
    sql: ${TABLE}.feature_name ;;
  }

  dimension: feature_value {
    type: string
    sql: ${TABLE}.feature_value ;;
  }

  dimension: height {
    type: number
    sql: ${TABLE}.height ;;
  }

  dimension: hsn_code {
    type: string
    sql: ${TABLE}.hsn_code ;;
  }

  dimension: id_customized_product {
    type: number
    sql: ${TABLE}.id_customized_product ;;
  }

  dimension: id_feature {
    type: number
    sql: ${TABLE}.id_feature ;;
  }

  dimension: id_feature_value {
    type: number
    sql: ${TABLE}.id_feature_value ;;
  }

  dimension: id_product {
    type: number
    sql: ${TABLE}.id_product ;;
  }

  dimension: igst {
    type: number
    sql: ${TABLE}.igst ;;
  }

  dimension: is_default_sku {
    type: number
    sql: ${TABLE}.is_default_sku ;;
  }

  dimension: is_deleted {
    type: number
    sql: ${TABLE}.is_deleted ;;
  }

  dimension: is_enabled {
    type: number
    sql: ${TABLE}.is_enabled ;;
  }

  dimension: length {
    type: number
    sql: ${TABLE}.length ;;
  }

  dimension: product_active {
    type: number
    sql: ${TABLE}.product_active ;;
  }

  dimension: product_category {
    type: string
    sql: ${TABLE}.product_category ;;
  }

  dimension: product_name {
    type: string
    sql: ${TABLE}.product_name ;;
  }

  dimension: product_price {
    type: number
    sql: ${TABLE}.product_price ;;
  }

  dimension: product_reference {
    type: string
    sql: ${TABLE}.product_reference ;;
  }

  dimension: product_type {
    type: string
    sql: ${TABLE}.product_type ;;
  }

  dimension: sku_code {
    type: string
    sql: ${TABLE}.sku_code ;;
  }

  dimension_group: sku_created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.sku_created_at ;;
  }

  dimension: sku_price {
    type: number
    sql: ${TABLE}.sku_price ;;
  }

  dimension_group: sku_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.sku_updated_at ;;
  }

  dimension: super_category {
    type: string
    sql: ${TABLE}.super_category ;;
  }

  dimension: type_sku {
    type: number
    sql: ${TABLE}.type_sku ;;
  }

  dimension: vms_sla_state {
    type: string
    sql: ${TABLE}.vms_sla_state ;;
  }

  dimension: vms_sla_state_id {
    type: number
    sql: ${TABLE}.vms_sla_state_id ;;
  }

  dimension: weight {
    type: number
    sql: ${TABLE}.weight ;;
  }

  dimension: width {
    type: number
    sql: ${TABLE}.width ;;
  }

  measure: count {
    type: count
    drill_fields: [product_name, feature_name]
  }
}
