view: livspace_reports2_flat_warehouses {
  sql_table_name: flat_tables.livspace_reports2_flat_warehouses ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: address_id {
    type: number
    sql: ${TABLE}.address_id ;;
  }

  dimension: code {
    type: string
    sql: ${TABLE}.code ;;
  }

  dimension: is_vendor_warehouse {
    type: number
    sql: ${TABLE}.is_vendor_warehouse ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: total_area {
    type: number
    sql: ${TABLE}.total_area ;;
  }

  dimension: total_bins {
    type: number
    sql: ${TABLE}.total_bins ;;
  }

  dimension: total_volume {
    type: number
    sql: ${TABLE}.total_volume ;;
  }

  measure: count {
    type: count
    drill_fields: [id, name]
  }
}
