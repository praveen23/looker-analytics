view: livspace_reports2_flat_vendor_facility {
  sql_table_name: flat_tables.livspace_reports2_flat_vendor_facility ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: billing_address_id {
    type: number
    sql: ${TABLE}.billing_address_id ;;
  }

  dimension: calendar_id {
    type: number
    sql: ${TABLE}.calendar_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: created_by_id {
    type: number
    sql: ${TABLE}.created_by_id ;;
  }

  dimension: deleted {
    type: number
    sql: ${TABLE}.deleted ;;
  }

  dimension: email {
    type: string
    sql: ${TABLE}.email ;;
  }

  dimension: gst_tin {
    type: string
    sql: ${TABLE}.gst_tin ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: payment_conditions {
    type: string
    sql: ${TABLE}.payment_conditions ;;
  }

  dimension: self_managed {
    type: number
    sql: ${TABLE}.self_managed ;;
  }

  dimension: shipping_address_id {
    type: number
    sql: ${TABLE}.shipping_address_id ;;
  }

  dimension: short_code {
    type: string
    sql: ${TABLE}.short_code ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: tc {
    type: number
    sql: ${TABLE}.tc ;;
  }

  dimension: terms_and_conditions {
    type: string
    sql: ${TABLE}.terms_and_conditions ;;
  }

  dimension: tin {
    type: string
    sql: ${TABLE}.tin ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: updated_by_id {
    type: number
    sql: ${TABLE}.updated_by_id ;;
  }

  dimension: vendor_id {
    type: number
    sql: ${TABLE}.vendor_id ;;
  }

  dimension: vendor_manager {
    type: string
    sql: ${TABLE}.vendor_manager ;;
  }

  dimension: vendor_manager_uid {
    type: string
    sql: ${TABLE}.vendor_manager_uid ;;
  }

  measure: count {
    type: count
    drill_fields: [id, name]
  }
}
