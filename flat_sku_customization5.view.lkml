view: flat_sku_customization5 {
  sql_table_name: flat_tables.flat_sku_customization5 ;;

  dimension: code {
    type: string
    sql: ${TABLE}.code ;;
  }

  dimension: customization_field {
    type: string
    sql: ${TABLE}.customization_field ;;
  }

  dimension: field_id {
    type: number
    sql: ${TABLE}.field_id ;;
  }

  dimension: id_product {
    type: number
    sql: ${TABLE}.id_product ;;
  }

  dimension: material {
    type: string
    sql: ${TABLE}.material ;;
  }

  dimension: material_display_name {
    type: string
    sql: ${TABLE}.material_display_name ;;
  }

  dimension: value_id {
    type: number
    sql: ${TABLE}.value_id ;;
  }

  measure: count {
    type: count
    drill_fields: [material_display_name]
  }
}
