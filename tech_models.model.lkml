connection: "ls_analytics"

# include all the views
include: "*.view"

datagroup: tech_models_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: tech_models_default_datagroup

explore: flat_projects {}

# - explore: flat_address

# - explore: flat_assembly_formula

# - explore: flat_boq_items

# - explore: flat_boq_items_temp

# - explore: flat_boq_products

# - explore: flat_consignments

# - explore: flat_credit_transactions

# - explore: flat_grouped_wms_item_grn

# - explore: flat_grouped_wms_item_grn_view

# - explore: flat_hsn_rates

# - explore: flat_inventory

# - explore: flat_invoice_item_tax

# - explore: flat_invoice_item_tax_table

# - explore: flat_item_oms_vms_view

# - explore: flat_item_state

# - explore: flat_logistics

# - explore: flat_order_items

# - explore: flat_orders

explore: flat_part_projects {}

# - explore: flat_pincode_city

# - explore: flat_po_item

# - explore: flat_po_item_misc

# - explore: flat_poitem_assembly

# - explore: flat_poitem_to_poitem

# - explore: flat_products

explore: flat_project_collaborators {}

# - explore: flat_project_events {}

explore: flat_project_payments {}

explore: flat_project_settings {}

# - explore: flat_purchase_order

# - explore: flat_sku_customization5

# - explore: flat_temp_3level_assembly

# - explore: flat_turnkey_po_margin

# - explore: flat_vendor_facility

# - explore: flat_vendor_item

# - explore: flat_vms_current_poitem

# - explore: flat_vms_current_poitem1

# - explore: flat_warehouses

# - explore: sku_customization_data
