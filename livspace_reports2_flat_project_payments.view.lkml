view: livspace_reports2_flat_project_payments {
  sql_table_name: flat_tables.livspace_reports2_flat_project_payments ;;

  dimension: entity_id {
    type: number
    sql: ${TABLE}.entity_id ;;
  }

  dimension: entity_type {
    type: string
    sql: ${TABLE}.entity_type ;;
  }

  dimension: id_project {
    type: number
    sql: ${TABLE}.id_project ;;
  }

  dimension: project_gmv {
    type: number
    sql: ${TABLE}.project_gmv ;;
  }

  dimension: ten_percent_payment {
    type: number
    sql: ${TABLE}.ten_percent_payment ;;
  }

  dimension_group: ten_percent_payment {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.ten_percent_payment_date ;;
  }

  dimension: total_paid {
    type: number
    value_format_name: id
    sql: ${TABLE}.total_paid ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
