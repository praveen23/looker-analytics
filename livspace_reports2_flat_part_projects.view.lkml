view: livspace_reports2_flat_part_projects {
  sql_table_name: flat_tables.livspace_reports2_flat_part_projects ;;

  dimension: project_id {
    type: number
    sql: ${TABLE}.project_id ;;
  }

  dimension: shipping_address_id {
    type: number
    sql: ${TABLE}.shipping_address_id ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
