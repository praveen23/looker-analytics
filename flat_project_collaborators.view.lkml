view: flat_project_collaborators {
  sql_table_name: flat_tables.flat_project_collaborators ;;

  dimension: design_manager {
    type: string
    sql: ${TABLE}.design_manager ;;
  }

  dimension: design_manager_email {
    type: string
    sql: ${TABLE}.design_manager_email ;;
  }

  dimension: general_manager {
    type: string
    sql: ${TABLE}.general_manager ;;
  }

  dimension: general_manager_email {
    type: string
    sql: ${TABLE}.general_manager_email ;;
  }

  dimension: primary_cm {
    type: string
    sql: ${TABLE}.primary_cm ;;
  }

  dimension: primary_cm_email {
    type: string
    sql: ${TABLE}.primary_cm_email ;;
  }

  dimension: primary_designer {
    type: string
    sql: ${TABLE}.primary_designer ;;
  }

  dimension: primary_designer_email {
    type: string
    sql: ${TABLE}.primary_designer_email ;;
  }

  dimension: primary_gm {
    type: string
    sql: ${TABLE}.primary_gm ;;
  }

  dimension: primary_gm_email {
    type: string
    sql: ${TABLE}.primary_gm_email ;;
  }

  dimension: project_designer {
    type: string
    sql: ${TABLE}.project_designer ;;
  }

  dimension: project_designer_email {
    type: string
    sql: ${TABLE}.project_designer_email ;;
  }

  dimension: project_id {
    type: number
    sql: ${TABLE}.project_id ;;
  }

  dimension: project_manager {
    type: string
    sql: ${TABLE}.project_manager ;;
  }

  dimension: project_manager_email {
    type: string
    sql: ${TABLE}.project_manager_email ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
