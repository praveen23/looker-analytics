view: flat_assembly_formula {
  sql_table_name: flat_tables.flat_assembly_formula ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: deleted {
    type: number
    sql: ${TABLE}.deleted ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: quantity {
    type: number
    sql: ${TABLE}.quantity ;;
  }

  dimension: result_sku_code {
    type: string
    sql: ${TABLE}.result_sku_code ;;
  }

  dimension: sku_code {
    type: string
    sql: ${TABLE}.sku_code ;;
  }

  measure: count {
    type: count
    drill_fields: [id, name]
  }
}
