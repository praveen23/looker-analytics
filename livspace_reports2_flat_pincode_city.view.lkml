view: livspace_reports2_flat_pincode_city {
  sql_table_name: flat_tables.livspace_reports2_flat_pincode_city ;;

  dimension: managed_city {
    type: number
    sql: ${TABLE}.managed_city ;;
  }

  dimension: managed_warehouse {
    type: number
    sql: ${TABLE}.managed_warehouse ;;
  }

  dimension: pincode {
    type: number
    sql: ${TABLE}.pincode ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
