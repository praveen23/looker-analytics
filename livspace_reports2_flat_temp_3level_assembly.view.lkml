view: livspace_reports2_flat_temp_3level_assembly {
  sql_table_name: flat_tables.livspace_reports2_flat_temp_3level_assembly ;;

  dimension: id_1 {
    type: number
    sql: ${TABLE}.id_1 ;;
  }

  dimension: id_2 {
    type: number
    sql: ${TABLE}.id_2 ;;
  }

  dimension: id_3 {
    type: number
    sql: ${TABLE}.id_3 ;;
  }

  dimension: order_id_1 {
    type: number
    value_format_name: id
    sql: ${TABLE}.order_id_1 ;;
  }

  dimension: order_id_2 {
    type: number
    value_format_name: id
    sql: ${TABLE}.order_id_2 ;;
  }

  dimension: order_id_3 {
    type: number
    value_format_name: id
    sql: ${TABLE}.order_id_3 ;;
  }

  dimension: order_type_1 {
    type: string
    sql: ${TABLE}.order_type_1 ;;
  }

  dimension: order_type_2 {
    type: string
    sql: ${TABLE}.order_type_2 ;;
  }

  dimension: order_type_3 {
    type: string
    sql: ${TABLE}.order_type_3 ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
