view: flat_logistics {
  sql_table_name: flat_tables.flat_logistics ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: active {
    type: number
    sql: ${TABLE}.active ;;
  }

  dimension: allocation_type {
    type: string
    sql: ${TABLE}.allocation_type ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: current_location_address_id {
    type: number
    sql: ${TABLE}.current_location_address_id ;;
  }

  dimension: current_location_type {
    type: string
    sql: ${TABLE}.current_location_type ;;
  }

  dimension: deleted {
    type: number
    sql: ${TABLE}.deleted ;;
  }

  dimension: destination_city {
    type: string
    sql: ${TABLE}.destination_city ;;
  }

  dimension: destination_id {
    type: number
    sql: ${TABLE}.destination_id ;;
  }

  dimension: order_id {
    type: number
    sql: ${TABLE}.order_id ;;
  }

  dimension: packages {
    type: number
    sql: ${TABLE}.packages ;;
  }

  dimension: parent_id {
    type: number
    sql: ${TABLE}.parent_id ;;
  }

  dimension: po_item_id {
    type: number
    sql: ${TABLE}.po_item_id ;;
  }

  dimension: quantity {
    type: number
    sql: ${TABLE}.quantity ;;
  }

  dimension_group: shipment_requested {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.shipment_requested_date ;;
  }

  dimension: sku_code {
    type: string
    sql: ${TABLE}.sku_code ;;
  }

  dimension: source_id {
    type: number
    sql: ${TABLE}.source_id ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: transit_request_type {
    type: string
    sql: ${TABLE}.transit_request_type ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [id]
  }
}
