view: livspace_reports2_flat_poitem_assembly {
  sql_table_name: flat_tables.livspace_reports2_flat_poitem_assembly ;;

  dimension: assembly_location {
    type: number
    sql: ${TABLE}.assembly_location ;;
  }

  dimension: build_from_1 {
    type: number
    sql: ${TABLE}.build_from_1 ;;
  }

  dimension: build_from_2 {
    type: number
    sql: ${TABLE}.build_from_2 ;;
  }

  dimension: build_from_3 {
    type: number
    sql: ${TABLE}.build_from_3 ;;
  }

  dimension: build_type_1 {
    type: number
    sql: ${TABLE}.build_type_1 ;;
  }

  dimension: build_type_2 {
    type: number
    sql: ${TABLE}.build_type_2 ;;
  }

  dimension: build_type_3 {
    type: number
    sql: ${TABLE}.build_type_3 ;;
  }

  dimension: component_po_item_id {
    type: number
    sql: ${TABLE}.component_po_item_id ;;
  }

  dimension: po_item_id {
    type: number
    sql: ${TABLE}.po_item_id ;;
  }

  dimension: po_item_id_1 {
    type: number
    value_format_name: id
    sql: ${TABLE}.po_item_id_1 ;;
  }

  dimension: po_item_id_2 {
    type: number
    value_format_name: id
    sql: ${TABLE}.po_item_id_2 ;;
  }

  dimension: po_item_id_3 {
    type: number
    value_format_name: id
    sql: ${TABLE}.po_item_id_3 ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
